package test.bat;

import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import main.java.base.TestBase;
import main.java.flows.csdb.CSDBPatientDetailsFlow;
import main.java.flows.proctor.ProctorFlow;
import main.java.flows.proctor.ProctorVirtualLabFlow;
import main.java.flows.www.CancelationFlow;
import main.java.flows.www.RegistrationFlow;
import main.java.flows.www.ShoppingFlow;
import main.java.flows.www.UserVirtualLabFlow;
import main.java.pages.proctor.ProctorLandingPage;
import main.java.pages.proctor.ProctorVirtualRoomPage;
import main.java.pages.www.EMedLoginPage;
import main.java.utils.CommonFunctions;
import main.java.utils.InvokeBrowser;
import main.java.utils.reportutil.ExtentManager;
import main.java.utils.xlservice.TestUtil;
import main.java.utils.xlservice.XlsReader;

/**
 * @author Mohammed Mustafa
 * 
 *         This class contains eMed BAT scenarios.
 */
@Listeners(main.java.utils.reportutil.TestListener.class)
public class BAT extends TestBase {

	private static boolean emedAccount = false;
	private static boolean navicaAccount = false;

	@BeforeClass
	public void checkTestSkip() throws Exception {
		count = -1;
		initialize();
		sheetName = "BAT";
		if (!TestUtil.isTestCaseRunnable(AAxls, sheetName)) {
			isExcelSkip = true;
			throw new SkipException("Skipping Test Case as runmode set to NO");// reports
		}
	}

	/**
	 * This method is to skip the scenario is not required in prod execution
	 * 
	 * @throws Exception
	 */
	public static void checkScenarioProdRunnable() throws Exception {
		if (env.equalsIgnoreCase("prod")) {
			if (CommonFunctions.getdata("ScenarioTitle").contains("eMed Kit Purchase")
					|| CommonFunctions.getdata("ScenarioTitle").contains("Order Cancellation")
					|| CommonFunctions.getdata("ScenarioTitle").contains("Proctor virtual lab session")
					|| CommonFunctions.getdata("ScenarioTitle").contains("Customer database")
					|| CommonFunctions.getdata("ScenarioTitle").contains("Dependent Patient Virtual Lab Session")) {
				CommonFunctions.logMessageSkipExecution("Scenario is not allowed for automation to run in prod env");
			}
		}
	}

	/**
	 * Test Method contains bat test-cases
	 * 
	 * @param data
	 * @throws Exception
	 */
	@Test(dataProvider = "getTestData")
	public void wwwBat(XlsReader data) throws Exception {

		inputdata = data;
		count++;
		isExcelSkip = false;
		test = extent.createTest(CommonFunctions.getdata("Scenario"));

		if (!CommonFunctions.getdata("RunMode").equalsIgnoreCase("Y")) {
			skip = true;
			CommonFunctions
					.logMessageSkipExecution("Excel Runmode set by user as no.\nPlease check the test data sheet");
		}
		checkScenarioProdRunnable();

		ExtentManager.scenarioLog("****" + CommonFunctions.getdata("Iteration") + "."
				+ CommonFunctions.getdata("Scenario") + " started****");

		switch (CommonFunctions.getdata("Iteration")) {

		/**
		 * This scenario is to Verify the patient is able to sign-up eMed account in
		 * eMed web-site.
		 **/
		case "1":
			InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
			emailId = CommonFunctions.EmailGenTimeStamp("emed.nav+", "gmail.com");
			RegistrationFlow.eMedAccountSignUpFlow(emailId, CommonFunctions.getdata("Password"));
			RegistrationFlow.fillRegisterSetup();
			emedAccount = true;
			if (!env.equalsIgnoreCase("prod")) {
				new EMedLoginPage().clickSignout();
			}
			break;

		/**
		 * This scenario is to Verify the patient is able to purchase a kit using
		 * existing eMed account
		 */

		case "2":
			/**
			 * This creates a new eMed user through rest api's if previous user creation
			 * scenario fails
			 */
			if (!emedAccount) {
				emailId = CommonFunctions.EmailGenTimeStamp("emed.nav+", "gmail.com");
				RegistrationFlow.eMedAPIUserCreation(emailId, CommonFunctions.getdata("Password"));
				emedAccount = true;
			}

			if (emedAccount) {
				InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
				RegistrationFlow.eMedGetTestloginFlow(emailId, CommonFunctions.getdata("Password"));
				ShoppingFlow.bookingTestKitFlow();
				new EMedLoginPage().clickSignout();
			} else {
				CommonFunctions.logMessageSkipExecution("No eMed account is created successfully.\nScenario skipped");
			}
			break;

		/**
		 * This scenario is to Verify whether the patient profile and order details are
		 * reflected in Customer database
		 */

		case "3":

			if (!confirmationNum.equals("")) {
				InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
				CSDBPatientDetailsFlow.loginCSDBPage();
				CSDBPatientDetailsFlow.verifyCustomerDetails(emailId);
			} else {
				CommonFunctions.logMessageSkipExecution(
						"No confirmation number is generated while purchase kit.\nScenario skipped");
			}
			break;

		/**
		 * This scenario is to Verify the patient is able to cancel kit order.
		 */

		case "4":
			if (!confirmationNum.equals("")) {
				InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
				RegistrationFlow.eMedGetTestloginFlow(emailId, CommonFunctions.getdata("Password"));
				CancelationFlow.cancelOpenOrderFlow();
				new EMedLoginPage().clickSignout();
			} else {
				CommonFunctions.logMessageSkipExecution(
						"No confirmation number is generated while purchase kit.\nScenario skipped");
			}
			break;

		/**
		 * This scenario is to Verify the patient is able to reset the eMed account
		 * password
		 */

		case "5":
			if (emedAccount) {
				InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
				RegistrationFlow.eMedAccountForgetPasswordFlow(emailId, CommonFunctions.getdata("ResetPassword"));
				RegistrationFlow.eMedGetTestloginFlow(emailId, CommonFunctions.getdata("ResetPassword"));
				new EMedLoginPage().verifyPageLogin();
			} else {
				CommonFunctions.logMessageSkipExecution("No eMed account is created successfully.\nScenario skipped");
			}
			break;

		/**
		 * This scenario is to Verify the patient is able to create new navica account
		 * on eMed website.
		 */

		case "6":
			InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
			navicaEmail = CommonFunctions.EmailGenTimeStamp("emed-au", "hostux.ninja");
			RegistrationFlow.navicaAccountSignupFlow(navicaEmail, CommonFunctions.getdata("Password"));
			new EMedLoginPage().verifyLoginPage();
			navicaAccount = true;
			break;

		/**
		 * This scenario is to Verify the patient is able to login and take assessment
		 * in eMed website
		 */

		case "7":
			if (navicaAccount) {
				InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
				RegistrationFlow.navicaLogin(navicaEmail, CommonFunctions.getdata("Password"));
				UserVirtualLabFlow.verifyUserAttendSessionAssessment();
			} else {
				CommonFunctions.logMessageSkipExecution("No navica account is created successfully.\nScenario skipped");
			}
			break;

		/**
		 * This scenario is to Verify whether the patient is able to reset the navica
		 * account password
		 */

		case "8":
			if (navicaAccount) {
				InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
				RegistrationFlow.navicaForgetPasswordFlow(navicaEmail, CommonFunctions.getdata("ResetPassword"));
				new EMedLoginPage().verifyPageLogin();
			} else {
				CommonFunctions.logMessageSkipExecution("No navica account is created successfully.\nScenario skipped");
			}
			break;

		/**
		 * This scenario is to Verify the proctor is able to login and view the account
		 * details
		 */

		case "9":
			if (BrowserNeed.equalsIgnoreCase("chrome")) {
				InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
				ProctorFlow.ProctorLoginProcess();
				ProctorFlow.verifyProctorMyAccount(proctorUserCred);
				new ProctorLandingPage().clickProctorSignout();
			} else {
				CommonFunctions.logMessageSkipExecution(
						"Proctor app is accessible only through chrome browser.\nScenario skipped");
			}
			break;

		/**
		 * This scenario is to Verify whether the proctor is able to handle virtual lab
		 * session with patient
		 */

		case "10":
			InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
			ProctorVirtualLabFlow.proctorVisitRoom();
			new ProctorVirtualRoomPage().verifyPatientInQueue();
			ProctorVirtualLabFlow.proctorVirtualAccept();
			ProctorVirtualLabFlow.startProctorTestProcess();
			new ProctorLandingPage().clickProctorSignout();
			break;

		/**
		 * This scenario is to Verify whether the patient as dependent is able to
		 * navigate virtual lab session
		 */

		case "11":
			InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
			ProctorVirtualLabFlow.proctorVisitRoom();
			UserVirtualLabFlow.connectAuthorizedPatientLabSession(3);
			break;

		/*
		 * case "12":
		 * InvokeBrowser.invokeApplication(CommonFunctions.getdata("Platform"));
		 * ProctorVirtualLabFlow.proctorVisitRoom(); new
		 * LandingPage().invokeWWWNewWindow();
		 * RegistrationFlow.navicaLogin(CommonFunctions.getdata("UserName"),
		 * CommonFunctions.getdata("Password"));
		 * UserVirtualLabFlow.verifyUserAttendSessionAssessment();
		 * ProctorVirtualLabFlow.proctorVirtualAccept(); new
		 * DualDriverTestStart().driverInvoker(""); new
		 * DualDriverTestStart().navicalogin("seme84097@gmail.com", "P@55w0rd12345");
		 * new DualDriverTestStart().startSessionTest();
		 * 
		 * ProctorVirtualLabFlow.proctorVirtualAccept();
		 * ProctorVirtualLabFlow.startProctorTestProcess(); driver1.close();
		 * CommonFunctions.moveToSpecifiedWindow(driver, 1);
		 * ProctorVirtualLabFlow.startProctorTestProcess();
		 * 
		 * new ProctorLandingPage().clickProctorSignout(); break;
		 */

		default:
			CommonFunctions.logErrorMessage("No such Scenarios exists please check the scenario name.");
		}

		ExtentManager.scenarioLog("****" + CommonFunctions.getdata("Iteration") + "."
				+ CommonFunctions.getdata("Scenario") + " Finished****");
		extent.flush();
	}

}