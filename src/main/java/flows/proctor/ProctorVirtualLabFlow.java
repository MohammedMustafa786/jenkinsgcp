package main.java.flows.proctor;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.pages.proctor.ProctorLandingPage;
import main.java.pages.proctor.ProctorVirtualRoomPage;
import main.java.utils.CommonFunctions;

public class ProctorVirtualLabFlow extends TestBase {

	private JavascriptExecutor jse = null;

	public ProctorVirtualLabFlow() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * 
	 * This method to click proctor visit room
	 * 
	 * @throws Exception
	 *
	 */

	public static void proctorVisitRoom() throws Exception {
		ProctorFlow.ProctorLoginProcess();
		new ProctorLandingPage().clickVirtualWaitingRoom();
	}

	/**
	 * This method to click the proctor Virtual Accept
	 * 
	 * @throws Exception
	 *
	 */

	public static void proctorVirtualAccept() throws Exception {
		CommonFunctions.moveToSpecifiedWindow(driver, 0);
		new ProctorVirtualRoomPage().proctorVirtualAccept();
		CommonFunctions.switchNextWindow(driver);
		driver.manage().window().maximize();
		new ProctorVirtualRoomPage().clickMuteButton();
	}

	/**
	 * This method to start the Proctor test process
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public static void startProctorTestProcess() throws Exception {
//		new ProctorVirtualRoomPage().verifySessionStarted(CommonFunctions.getdata("FirstName"),
//				CommonFunctions.getdata("LastName"));
		boolean flag = true;
		int stepCount = 1;
		while (flag) {
			new ProctorVirtualRoomPage().clickNextStepButton();
			CommonFunctions.logMessage("Step " + (stepCount++) + " is completed successfully");
			if (driver.findElements(By.xpath("//*[text()='Next']//ancestor::button[@disabled]")).size() > 0) {
				new ProctorVirtualRoomPage().selectTestStatus(CommonFunctions.getdata("ResultStatus"));
				new ProctorVirtualRoomPage().clickSubmitButton();
				new ProctorVirtualRoomPage().confirmSubmitResult();
				new ProctorVirtualRoomPage().clickEndCallButton();
				new ProctorVirtualRoomPage().verifySessionEnded();
				driver.close();
				if (CommonFunctions.getCurrentWindowsCount(driver) == 2) {
					CommonFunctions.moveToSpecifiedWindow(driver, 1);
					driver.close();
				}

				CommonFunctions.moveToSpecifiedWindow(driver, 0);
				flag = false;
			}
		}

	}

	/**
	 * Method used to remove patient from queue and connect with certified patients
	 * 
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	public static boolean verifyCertifiedPatient(String fName, String lName) throws Exception {

		boolean startFlag = true;

		startFlag = CommonFunctions.isExist(driver, "//*[text()='" + fName + " " + lName + "']");
		if (startFlag) {
			CommonFunctions.logMessage("Virtual lab session has started with official patient");
		} else {
			driver.close();
			CommonFunctions.moveToSpecifiedWindow(driver, 0);
		}
		return startFlag;

	}

}
