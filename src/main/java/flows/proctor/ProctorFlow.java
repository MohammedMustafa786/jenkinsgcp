package main.java.flows.proctor;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import encryptusercredentials.EncryptCredentails;
import main.java.base.TestBase;
import main.java.pages.proctor.ProctorLandingPage;
import main.java.pages.proctor.ProctorLoginPage;
import main.java.pages.proctor.ProctorMyAccountPage;
import main.java.utils.CommonFunctions;

public class ProctorFlow extends TestBase {

	private JavascriptExecutor jse = null;

	public ProctorFlow() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * 
	 * This method for Proctor Login
	 * 
	 * @param uName
	 * @param password
	 * @throws Exception
	 *
	 */

	public static void ProctorLoginProcess() throws Exception {
		if (env.equalsIgnoreCase("prod")) {
			if (proctorCredentials != null) {
				proctorUserCred = proctorCredentials.split(":")[0];
				proctorPasswordCred = proctorCredentials.split(":")[1];
			} else {
				CommonFunctions.logErrorMessagestopExecution("Proctor creds not provided for the env " + env);
			}
		} else {
			proctorUserCred = new EncryptCredentails()
					.decrypt(CommonFunctions.getPropertyValues().getProperty("proctorusername"));
			proctorPasswordCred = new EncryptCredentails()
					.decrypt(CommonFunctions.getPropertyValues().getProperty("proctorpassword"));
		}
		new ProctorLoginPage().verifyProctorLoginPage();
		new ProctorLoginPage().enterProctorUser(proctorUserCred);
		new ProctorLoginPage().enterProctorPassword(proctorPasswordCred);
		new ProctorLoginPage().clickProctorLogin();
		new ProctorLandingPage().verifyProctorLandingPage();
	}

	/**
	 * 
	 * Proctor account verification method
	 * 
	 * @param proctorEmail
	 * @throws Exception
	 *
	 */

	public static void verifyProctorMyAccount(String proctorEmail) throws Exception {
		new ProctorLandingPage().clickMyAccount();
		new ProctorMyAccountPage().verifyMyAccountPageDisplayed();
		new ProctorMyAccountPage().verifyProctorDetails(proctorEmail);
	}

}
