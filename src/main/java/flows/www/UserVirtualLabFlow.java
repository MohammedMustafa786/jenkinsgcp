package main.java.flows.www;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.flows.proctor.ProctorVirtualLabFlow;
import main.java.pages.proctor.ProctorLandingPage;
import main.java.pages.proctor.ProctorVirtualRoomPage;
import main.java.pages.www.LandingPage;
import main.java.pages.www.PatientVirtualRoomPage;
import main.java.utils.CommonFunctions;

public class UserVirtualLabFlow extends TestBase {

	private JavascriptExecutor jse = null;

	public UserVirtualLabFlow() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * Virtual Lab session method
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public static void verifyUserAttendSessionAssessment() throws Exception {

		if (CommonFunctions.getdata("Dependent").equalsIgnoreCase("Y")) {
			new PatientVirtualRoomPage().selectDependentPatient();
		}
		new PatientVirtualRoomPage().virtualLabTestQuestion();
		new PatientVirtualRoomPage().clickPreTestTerms();
		new PatientVirtualRoomPage().clickPreTestContinue();
		new PatientVirtualRoomPage().verifyVirtualLabSession();
	}

	/**
	 * Method is used to take a virtual session with authorized patient
	 * 
	 * @throws Exception
	 */
	public static void connectAuthorizedPatientLabSession(int tries) throws Exception {

		boolean stopFlag = true;
		int count = 0;

		for (int proctorTry = 0; proctorTry <= tries; proctorTry++) {

			CommonFunctions.moveToSpecifiedWindow(driver, 0);
			stopFlag = new ProctorVirtualRoomPage().removePatientInQueueAndAcceptCertifiedPatient();

			if (proctorTry != 0 && !stopFlag) {
				CommonFunctions.switchNextWindow(driver);
				driver.close();
				CommonFunctions.moveToSpecifiedWindow(driver, 0);
			}

			if (proctorTry < tries) {

				if (!stopFlag) {

					new LandingPage().invokeWWWNewWindow();

					if (count == 0) {
						RegistrationFlow.navicaLogin(CommonFunctions.getdata("UserName"),
								CommonFunctions.getdata("Password"));
						count++;
					} else {
						new LandingPage().verifyLandingPage();
						new LandingPage().clickStartTesting();
					}

					UserVirtualLabFlow.verifyUserAttendSessionAssessment();

				} else {
					CommonFunctions.logMessage("Connected with the authorized patient");
					ProctorVirtualLabFlow.startProctorTestProcess();
					new ProctorLandingPage().clickProctorSignout();
					break;
				}
			}
		}

		if (!stopFlag) {
			CommonFunctions.logErrorMessagestopExecution(
					"Tired "+tries+" times for virtual lab session but failed to get an patient in queue");
		}
	}

}
