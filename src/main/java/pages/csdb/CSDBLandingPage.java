package main.java.pages.csdb;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class CSDBLandingPage extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(id = "email")
	private WebElement emailCSDB;

	@FindBy(id = "password")
	private WebElement passwordCSDB;

	@FindBy(xpath = "//*[text()='Login']")
	private WebElement LoginCSDB;

	@FindBy(xpath = "//*[@id='header']//following::h1")
	private WebElement landingPageHeader;

	@FindBy(xpath = "//a[contains(text(),'Customer')]")
	private WebElement customerDatabase;
	
	@FindBy(xpath = "//*[@href='/order']")
	private WebElement orderDatabase;

	@FindBy(xpath = "//input[@placeholder='Search by name, email, or order ID']")
	private WebElement userSearchBox;

	@FindBy(xpath = "(//tbody//tr//td)[3]")
	private WebElement firstCustomer;

	@FindBy(xpath = "//tbody//tr//td")
	private List<WebElement> userSearchResults;

	public CSDBLandingPage() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * This Method for verifying the CSDB Page
	 * @throws Exception 
	 *
	 */

	public void verifyCSDBPage() throws Exception {
		CommonFunctions.checkCurrentPageTitle("eMed Admin");
		CommonFunctions.logMessage("<-----CSDB Login Page----->");
	}

	/**
	 * This Method to enter the CSDB mail id
	 * 
	 * @param email
	 * 
	 * @throws Exception
	 * 
	 *
	 */

	public void enterCSDBLogin(String email) throws Exception {
		CommonFunctions.SendkeysWithoutInputLog(emailCSDB, email, "CSDB username text box");
	}

	/**
	 * This Method to enter the CSDB password
	 * 
	 * @param password
	 * 
	 * @throws Exception
	 * 
	 *
	 */

	public void enterCSDBPassword(String password) throws Exception {
		CommonFunctions.SendkeysWithoutInputLog(passwordCSDB, password, "CSDB password text box");
	}

	/**
	 * This Method to click the CSDB login button
	 * 
	 * @throws Exception
	 *
	 */

	public void clickCSDBLogin() throws Exception {
		WebElement loginBtn = CommonFunctions.findElementByXpath(driver, "//*[text()='Login']", "CSDB Login button");
		CommonFunctions.actionClick(loginBtn, "CSDB Login button");
	}

	/**
	 * This Method to verify the Landing Page
	 * 
	 *
	 * @throws Exception
	 */

	public void verifyLandingPage() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		CommonFunctions.elementIsVisible(landingPageHeader, "CSDB Header");
		try {
			if (landingPageHeader.isDisplayed()) {
				CommonFunctions.logMessage("Successfully navigated to Customer Database Page");
			}
		} catch (Exception e) {
			CommonFunctions.logErrorMessagestopExecution("Error while navigating to CSDB Page");
		}
	}

	/**
	 * This Method to click the customer database page
	 * 
	 *
	 * @throws Exception
	 */

	public void clickCustomerDataBase() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		CommonFunctions.actionClick(customerDatabase, "customer database");
	}
	
	/**
	 * This Method to click the order database page
	 * 
	 *
	 * @throws Exception
	 */

	public void clickOrderDataBase() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		CommonFunctions.actionClick(orderDatabase, "order database");
	}

	/**
	 * This Method verify the CSDB user list is displayed or not
	 * 
	 *
	 * @throws Exception
	 */

	public void verifyCustomerDataList() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		CommonFunctions.elementIsVisible(firstCustomer, "CSDB list");
		try {
			if (firstCustomer.isDisplayed()) {
				CommonFunctions.logMessage("Customer database List is displayed");
			}
		} catch (Exception e) {
			CommonFunctions.logErrorMessagestopExecution("No Data displayed in customer database page");
		}
	}

	/**
	 * This Method to search the CSDB user
	 * 
	 *
	 * @throws Exception
	 */

	public void enterCSDBUserSearch(String searchValue) throws Exception {
		CommonFunctions.Sendkeys(userSearchBox, searchValue, "CSDB patient search text box");
	}

	/**
	 * This Method click the CSDB First customer
	 * 
	 *
	 * @throws Exception
	 */

	public void clickCSDBFirstList() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		CommonFunctions.actionClick(firstCustomer, "CSDB first customer");
	}

}
