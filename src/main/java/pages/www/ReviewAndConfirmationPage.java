package main.java.pages.www;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class ReviewAndConfirmationPage extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(xpath = "//h1[text()='Order Placed']")
	private WebElement orderPlaced;

	@FindBy(xpath = "//button[@data-testid='place-order-button']")
	private WebElement placeYOrder;

	@FindBy(xpath = "//p[contains(text(),'Confirmation')]")
	private WebElement confirmNumber;

	public ReviewAndConfirmationPage() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;
	
	/**
	 * Method to click the place your order
	 * 
	 * @throws Exception
	 */

	public void clickPlaceYourOrder() throws Exception {
		CommonFunctions.elementToBeClickable(placeYOrder, "Place your order button");
		WebElement placeYourOrder = driver.findElement(By.xpath("//button[@data-testid='place-order-button']"));
		placeYourOrder.click();
		CommonFunctions.logMessage("Place your order button is clicked");
	}
	
	/**
	 * Order placed verification method
	 * 
	 * @throws Exception
	 */

	public void verifyOrderPlaced() throws Exception {
		CommonFunctions.elementIsVisible(orderPlaced, "order placed header");
		confirmationNum = CommonFunctions.getTextOfElement(confirmNumber, "confirmation number").split("#")[1];
		if (!confirmationNum.equals("")) {
			CommonFunctions.logMessage("Confirmation number for the placed order: " + confirmationNum);
		} else {
			CommonFunctions.logErrorMessagestopExecution("No confirmation number is generated");
		}
	}
	
	/**
	 * Method get the order id for order placed
	 *
	 */
	public void getOrderId() {
		
		eMedId = driver.getCurrentUrl().split("orderId=")[1];
		CommonFunctions.logMessage("Order Id for the order placed: " + eMedId);
	}

}
