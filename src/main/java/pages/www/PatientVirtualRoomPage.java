package main.java.pages.www;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class PatientVirtualRoomPage extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(xpath = "//h1[contains(text(),'This test will take')]")
	private WebElement twentyMinutesTestHeader;

	@FindBy(xpath = "//form//div[@data-testid]//p")
	private List<WebElement> preTestQuestions;

	@FindBy(tagName = "label")
	private List<WebElement> answers;

	@FindBy(xpath = "//label[@for='dateOfBirth']")
	private WebElement DOBQues;

	@FindBy(id = "dateOfBirth")
	private WebElement DOBText;

	@FindBy(xpath = "//label[@for='preflight-terms-conditions']")
	private WebElement preTestTerms;

	@FindBy(xpath = "//button[text()='CONTINUE']")
	private WebElement pretestContinue;

	@FindBy(xpath = "//p[contains(text(),'Waiting on a Certified Guide')]")
	private WebElement waitForProctor;

	@FindBy(xpath = "//*[contains(@class,'dropdown__indicators')]")
	private WebElement dependentDropdown;

	@FindBy(xpath = "//*[contains(@id,'react-select-2-option')]")
	private List<WebElement> dependentList;

	public PatientVirtualRoomPage() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * Method to check Navica Permission
	 * 
	 *
	 * @throws Exception
	 * 
	 */

	public void preTestDOB(String DOB) throws Exception {
		CommonFunctions.logMessage(CommonFunctions.getTextOfElement(DOBQues, "DOB pretest question"));
		DOBText.sendKeys(DOB, Keys.ENTER);
		CommonFunctions.logMessage("DOB entered as " + DOB);
	}

	/**
	 * Virutal Lab Test Question
	 * 
	 *
	 * @throws Exception
	 * 
	 */

	public void virtualLabTestQuestion() throws Exception {
		CommonFunctions.elementIsVisible(twentyMinutesTestHeader, "twenty minutes header");
		for (int ques = 0; ques < 9; ques++) {
			CommonFunctions.logMessage("Pre-Test Assess " + (ques + 1) + ": "
					+ CommonFunctions.getTextOfElement(preTestQuestions.get(ques), "pre-test questions"));
			List<WebElement> preTestAnswer = driver
					.findElements(By.xpath("(//form//div[@data-testid])[" + (ques + 1) + "]//label"));
			String answer = CommonFunctions.getTextOfElement(preTestAnswer.get(0), "pretest answer check button");
			CommonFunctions.clickWebelement(preTestAnswer.get(0), answer);
		}
	}

	/**
	 * This method is used to click the terms and authorization in pre-test
	 * 
	 * @throws Exception
	 */
	public void clickPreTestTerms() throws Exception {
		boolean flag = CommonFunctions.isExist(driver, "//label[@for='preflight-terms-conditions']");
		if (flag) {
			CommonFunctions.clickWebelement(preTestTerms, "Pre-test terms and authorization");
		}

	}

	/**
	 * Method to click the Pretest continue in Proctor
	 * 
	 *
	 * @throws Exception
	 * 
	 */

	public void clickPreTestContinue() throws Exception {
		CommonFunctions.elementToBeClickable(pretestContinue, "pre-test continue button");
		WebElement preAssessContinue = driver.findElement(By.xpath("//button[text()='CONTINUE']"));
		CommonFunctions.elementToBeClickable(preAssessContinue, "pre-test assess button");
		preAssessContinue.click();
		CommonFunctions.logMessage("Pre-Test continue button is clicked");

	}

	/**
	 * Method to verify the Virtual Lab session in Proctor
	 * 
	 *
	 * @throws Exception
	 * 
	 */

	public void verifyVirtualLabSession() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		CommonFunctions.elementVisibleToCheck(waitForProctor, 25);
		CommonFunctions.logMessage("Virtual lab page is displayed and waiting for certified proctor");
	}

	/**
	 * Method to select the dependent patient from the dropdown shown
	 * 
	 * @throws Exception
	 */
	public void selectDependentPatient() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		CommonFunctions.elementIsVisible(twentyMinutesTestHeader, "twenty minutes header");
		CommonFunctions.logMessage("Who will be taking the test?");
		CommonFunctions.actionClick(dependentDropdown, "dependent dropdown");
		CommonFunctions.elementIsVisible(dependentList.get(0), "dependent list");
		if (dependentList.size() > 1) {
			int dependentNum = dependentList.size() - 1;
			dependentPatientName = CommonFunctions.getTextOfElement(dependentList.get(dependentNum),
					"dependent patient");
			CommonFunctions.logMessage("Patient selected for the session " + dependentPatientName);
			CommonFunctions.actionClick(dependentList.get(dependentNum), "dependent patient");

		} else {
			CommonFunctions.logErrorMessagestopExecution("Dependent patient is not available for this navica account");
		}

	}

}
