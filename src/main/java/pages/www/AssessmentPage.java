package main.java.pages.www;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class AssessmentPage extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(xpath = "//h1[text()='Eligibility Assessment']|//h1[contains(text(),'COVID-19')]")
	private WebElement eligibilityAssessment;

	@FindBy(xpath = "//button[text()='Start Assessment']|//button[text()='Start Now']")
	private WebElement StartAssessment;

	@FindBy(xpath = "//h1[contains(text(),'COVID-19 Testing Questionnaire')]//following::p")
	private WebElement assessDeviceCheck;

	@FindBy(xpath = "//p[contains(text(),'COVID-19 Testing Questionnaire')]//parent::div//form//p")
	private WebElement eligibilityQuestions;

	@FindBy(tagName = "label")
	private List<WebElement> answers;

	@FindBy(xpath = "//p[contains(text(),'Assessment Complete')]|//p[text()='Questionnaire Complete']")
	private WebElement assessmentComplete;
	
	@FindBy(xpath = "//input[@id='date-of-birthday']")
	private WebElement dob;

	public AssessmentPage() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * Method to click the start Assessment
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public void clickStartAssessment() throws Exception {

		CommonFunctions.elementIsVisible(eligibilityAssessment, "eligibility assessment module");
		CommonFunctions.waitForPageLoad(driver);
		WebElement startAssessment = driver.findElement(By.xpath("//button[text()='Start Now']"));
		startAssessment.click();
		CommonFunctions.logMessage("start assessment button is clicked");
	}

	/**
	 * Method to check the assessment device check
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public void assessmentDeviceCheck() throws Exception {
		CommonFunctions.logMessage(
				"Device Check: " + CommonFunctions.getTextOfElement(assessDeviceCheck, "device assessment check"));
		CommonFunctions.scrollIntoView(answers.get(0));
		String answer = CommonFunctions.getTextOfElement(answers.get(0), "next button");
		CommonFunctions.clickWebelement(answers.get(0), answer);
		clickNextQuestion();
	}

	/**
	 * Assessment answering method
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public void answerTheQuestions(int quesCount) throws Exception {
		for (int ques = 1; ques < quesCount; ques++) {
			CommonFunctions.logMessage("Eligibilty question " + ques + ": "
					+ CommonFunctions.getTextOfElement(eligibilityQuestions, "eligibility questions"));
			if (ques == 1) {
				String answer = CommonFunctions.getTextOfElement(answers.get(0), "answer check button");
				CommonFunctions.clickWebelement(answers.get(0), answer);
			} else {
				String answer = CommonFunctions.getTextOfElement(answers.get(0), "answer check button");
				CommonFunctions.clickWebelement(answers.get(0), answer);
			}
			clickNextQuestion();
		}
	}

	/**
	 * Method to click the next question in assessment
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public void clickNextQuestion() throws Exception {
		WebElement nextQuestion = driver.findElement(By.xpath("//button[contains(text(),'Next Question')]"));
		CommonFunctions.elementToBeClickable(nextQuestion, "next question button");
		nextQuestion.click();
		CommonFunctions.logMessage("Next question is clicked");
	}

	/**
	 * Assessment completed verification method
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public void verifyAssessmentCompleted() throws Exception {
		CommonFunctions.elementToBeClickable(assessmentComplete, "assessment complete popup");
		if (assessmentComplete.isDisplayed()) {
			CommonFunctions.logMessage("Assessment completed successfully");
			closeAssessmentComplete();
		}
	}

	/**
	 * Method to close the Assessment
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public void closeAssessmentComplete() throws Exception {
		WebElement closeAssess = driver.findElement(By.xpath("//button[contains(text(),'Close')]"));
		CommonFunctions.elementToBeClickable(closeAssess, "next question button");
		closeAssess.click();
		CommonFunctions.logMessage("Close assessment is clicked");
	}
	
	/**
	 * DOB Assessment answering method
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public void answerTheQuestionsDob(String DOB) throws Exception {
		CommonFunctions.SendkeysAttrib(dob, "Value", DOB, "DOB");
		dob.sendKeys(Keys.ENTER);
		clickNextQuestion();

	}
}
