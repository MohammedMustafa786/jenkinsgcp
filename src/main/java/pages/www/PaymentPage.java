package main.java.pages.www;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class PaymentPage extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(xpath = "//*[contains(text(),'Please select and enter')]")
	private WebElement paymentPageHeader;

	@FindBy(id = "cardHolderName")
	private WebElement cardHolderName;

	@FindBy(xpath = "//input[@aria-label='Credit or debit card number']")
	private WebElement cardNumber;

	@FindBy(xpath = "//input[@aria-label='Credit or debit card expiration date']")
	private WebElement cardExpiryDate;

	@FindBy(xpath = "//input[@placeholder='CVC']")
	private WebElement cardCVVNumber;

	@FindBy(id = "cardHolderZipCode")
	private WebElement cardHolderZip;

	@FindBy(xpath = "(//*[@data-testid='order-summary-subtotal'])[2]")
	private WebElement orderSubTotal;

	@FindBy(xpath = "(//*[@data-testid='order-summary-shipping'])[2]")
	private WebElement orderShipRate;

	@FindBy(xpath = "(//*[@data-testid='order-summary-tax'])[2]")
	private WebElement orderTax;

	@FindBy(xpath = "(//*[@data-testid='order-summary-total'])[2]")
	private WebElement orderTotal;

	@FindBy(xpath = "//span[text()='Continue To Review']//ancestor::button")
	private WebElement continueToReview;

	public PaymentPage() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * Method to verify the payment page is displayed successfully
	 * 
	 * @throws Exception
	 */
	public void verifyPaymentPage() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		try {
			if (paymentPageHeader.isDisplayed()) {
				CommonFunctions.logMessage("Payment page is displayed successfully");
			}
		} catch (Exception e) {
			CommonFunctions.logErrorMessagestopExecution("Error while proceeding payment page");
		}
	}

	/**
	 * Method for enter the Card Holder Name
	 * 
	 * @param cardHoldName
	 * 
	 * @throws Exception
	 */

	public void enterCardHolder(String cardHoldName) throws Exception {
		CommonFunctions.Sendkeys(cardHolderName, cardHoldName, "card holder name");
	}

	/**
	 * Method for enter the Card Number
	 * 
	 * @param cNumber
	 * 
	 * @throws Exception
	 */

	public void enterCardNumber(String cNumber) throws Exception {
		driver.switchTo().frame(0);
		CommonFunctions.Sendkeys(cardNumber, cNumber, "card number");
		driver.switchTo().defaultContent();
	}

	/**
	 * Method for enter the Card Expiry Date
	 * 
	 * @param expiryDate
	 * 
	 * @throws Exception
	 */

	public void enterCardExpiryDate(String expiryDate) throws Exception {
		driver.switchTo().frame(1);
		CommonFunctions.Sendkeys(cardExpiryDate, expiryDate, "expiry date");
		driver.switchTo().defaultContent();
	}

	/**
	 * Method for enter the Card CVV
	 * 
	 * @param CVV
	 * 
	 * @throws Exception
	 */

	public void enterCardCVV(String CVV) throws Exception {
		driver.switchTo().frame(2);
		CommonFunctions.Sendkeys(cardCVVNumber, CVV, "CVV number");
		driver.switchTo().defaultContent();
	}

	/**
	 * Method for enter the Card zipcode
	 * 
	 * @param zipCode
	 * 
	 * @throws Exception
	 */

	public void enterCardHolderZipCode(String zipCode) throws Exception {
		CommonFunctions.Sendkeys(cardHolderZip, zipCode, "zip code");
	}

	/**
	 * Method to click the continue to Preview
	 * 
	 * @throws Exception
	 */

	public void clickContinueToReview() throws Exception {
		CommonFunctions.elementToBeClickable(continueToReview, "continue review button");
		WebElement continueToReview = driver
				.findElement(By.xpath("//span[text()='Continue To Review']//ancestor::button"));
		continueToReview.click();
		CommonFunctions.logMessage("Continue review button is clicked");
	}

	/**
	 * Method is used to get the shipping details from the order summary displayed
	 * 
	 * @throws Exception
	 */
	public void getShippingRateDetails() throws Exception {

		CommonFunctions.logMessage("Order summary details");
		orderSubRate = CommonFunctions.getTextOfElement(orderSubTotal, "order subtotal").replace("$", "");
		String shippingMethodPrice = CommonFunctions.getTextOfElement(orderShipRate, "order ship rate").replace("$",
				"");
		orderTaxRate = CommonFunctions.getTextOfElement(orderTax, "order tax rate").replace("$", "");
		orderTotalAmount = CommonFunctions.getTextOfElement(orderTotal, "order total").replace("$", "");

		if (shippingMethodPrice.equalsIgnoreCase(orderShippingRate)) {
			CommonFunctions.logMessage("Order shipping cost " + orderShippingRate);
		} else {
			CommonFunctions
					.logErrorMessagestopExecution("Error while validating shipping method price " + orderShippingRate);
		}

		CommonFunctions.logMessage("Order kit subtotal cost " + orderSubRate);
		CommonFunctions.logMessage("Order tax rate " + orderTaxRate);
		CommonFunctions.logMessage("Order total cost " + orderTotalAmount);
	}
}
