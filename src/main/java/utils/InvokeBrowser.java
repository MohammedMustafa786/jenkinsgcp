package main.java.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import encryptusercredentials.EncryptCredentails;

import org.openqa.selenium.Proxy;
import io.github.bonigarcia.wdm.WebDriverManager;
import main.java.base.TestBase;

public class InvokeBrowser extends TestBase {
	private static DesiredCapabilities dc;

	/**
	 * This method for invoke the Browser and pass the url to the browser
	 * 
	 * @param platform We have to pass the channel for invoking like www, proctor
	 *                 ,etc..
	 * 
	 * @throws Exception
	 */
	public static void invokeApplication(String platform) throws Exception {

		String baseurl = "";

		switch (platform) {

		case "WWW":

			switch (env.toLowerCase()) {
			case "stg":
			case "dev2":
				if (BrowserNeed.equalsIgnoreCase("chrome") || BrowserNeed.equalsIgnoreCase("firefox")) {
					baseurl = "https://"
							+ new EncryptCredentails()
									.decrypt(CommonFunctions.getPropertyValues().getProperty("wwwusername"))
							+ ":"
							+ new EncryptCredentails()
									.decrypt(CommonFunctions.getPropertyValues().getProperty("wwwpassword"))
							+ "@" + env + ".emed.com";

				} else if (BrowserNeed.equalsIgnoreCase("safari")) {
					baseurl = "https://" + env + ".emed.com";
				}
				break;

			case "prod":
				baseurl = "https://emed.com/app";
				break;

			case "training":
				baseurl = "https://training.emed.com/";
				break;

			default:
				CommonFunctions.logErrorMessagestopExecution("Incorrect environment is selected");
				break;

			}
			break;

		case "Proctor":
			if (!env.equalsIgnoreCase("prod")) {
				baseurl = "https://proctor-api." + env + ".emed.com/users/sign-in";
			} else {
				baseurl = "https://proctor-api.emed.com/users/sign-in";
			}
			break;

		case "CSDB":
			if (!env.equalsIgnoreCase("prod")) {
				baseurl = "https://admin." + env + ".emed.com";
			} else {
				baseurl = "https://admin.emed.com";
			}
			break;

		default:
			CommonFunctions.logErrorMessagestopExecution("Incorrect platform selected");
			break;
		}

		/*
		 * Closing the browser of the previous scenario executed
		 */
		if (isFirst != 1) {
			driver.quit();
		} else {
			isFirst++;
		}

		try {
			if (BrowserNeed.equalsIgnoreCase("firefox")) {
				
				WebDriverManager.firefoxdriver().setup();
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
				capabilities.setCapability("browserName", "firefox");
				capabilities.setCapability("marionette", true);
				capabilities.setCapability("acceptInsecureCerts", true);
				capabilities.setCapability("javascriptEnabled", true);
				driver = new FirefoxDriver(capabilities);

			} else if (BrowserNeed.equalsIgnoreCase("safari")) {
				if (System.getProperty("os.name").contains("OS X")) {

					System.setProperty("webdriver.safari.driver",
							System.getProperty("user.dir") + "/src/main/resources/drivers/SafariDriver.safariextz");
					driver = new SafariDriver();
				} else {
					System.err.println("Safari is only supported on MAC - OS X");
				}

			} else if (BrowserNeed.equalsIgnoreCase("chrome")) {
				String os = System.getProperty("os.name");
				CommonFunctions.logMessage("OS Name - " + os);
				if (os.contains("Windows") || os.contains("Mac")) {
					ChromeOptions options = new ChromeOptions();
					HashMap<String, Object> chromePref = new HashMap<String, Object>();
					chromePref.put("profile.default_content_settings.popups", 0);
					chromePref.put("download.default_directory", downloadFilePath);
					options.setExperimentalOption("prefs", chromePref);
					options.addArguments("disable-infobars");
					if (mode.equalsIgnoreCase("incog")) {
					System.out.println("Window Cognito User");
						options.addArguments("--incognito");
					}
					options.addArguments("use-fake-ui-for-media-stream");
					options.addArguments("test-type");
					options.addArguments("ignore-certificate-errors");
					options.setAcceptInsecureCerts(true);
					WebDriverManager.chromedriver().setup();
					driver = new ChromeDriver(options);

				} else if (os.contains("Linux")) {
					System.out.println("Linux");
					ChromeOptions options = new ChromeOptions();
					Map<String, String> chromePreferences = new HashMap<>();

					options.addArguments("disable-infobars");
					chromePreferences.put("profile.password_manager_enabled", "false");

					Proxy proxy = new Proxy();
					proxy.setProxyType(Proxy.ProxyType.DIRECT);
System.out.println("Linux Chrome User");
					options.addArguments("--headless");
					options.addArguments("--no-sandbox");
					options.addArguments("--disable-setuid-sandbox");
					options.addArguments("--disable-dev-shm-usage");
					options.addArguments("test-type");
					options.addArguments("ignore-certificate-errors");
					options.addArguments("--window-size=1325x744");
					options.setAcceptInsecureCerts(true);

					options.setCapability("chrome.switches", "--no-default-browser-check");
					options.setCapability("chrome.prefs", chromePreferences);
					options.setCapability(CapabilityType.PROXY, proxy);
					options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					options.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
					options.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);
					options.setCapability(ChromeOptions.CAPABILITY, options);
					// options.setCapability("name", testName);
				}

			}
			CommonFunctions.logMessage("Lauched the " + BrowserNeed + " browser Successfully....");
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(baseurl);

			if (BrowserNeed.equalsIgnoreCase("safari") && platform.equalsIgnoreCase("WWW")) {

				CommonFunctions.robotBrowserAuthenticate(
						new EncryptCredentails()
								.decrypt(CommonFunctions.getPropertyValues().getProperty("wwwusername")),
						new EncryptCredentails()
								.decrypt(CommonFunctions.getPropertyValues().getProperty("wwwpassword")));
			}
			CommonFunctions.logMessage("Navigated to the site " + baseurl);

		} catch (Exception e) {
			fail = true;
			e.printStackTrace();
			CommonFunctions.logErrorMessagestopExecution("Error while invoking browser");
		}

	}

}