package main.java.utils.xlservice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestUtil {

	// finds if the test suite is runnable
	/**
	 * @param xls this is the excel sheet which we have to read
	 * @param suiteName we have to give the sheet name which we have to read
	 * @return
	 */
	public static boolean isSuiteRunnable(XlsReader xls, String suiteName) {
		boolean isExecutable = false;
		// System.out.println(xls.getRowCount("App Name"));
		for (int i = 2; i <= xls.getRowCount("App Name"); i++) {
			String suite = xls.getCellData("App Name", "Application Name", i);
			String runmode = xls.getCellData("App Name", "Run Mode", i);

			if (suite.equalsIgnoreCase(suiteName)) {

				if (runmode.equalsIgnoreCase("Y")) {
					isExecutable = true;
				} else {
					isExecutable = false;
				}
			}

		}
		xls = null; // release memory
		return isExecutable;

	}

	// returns true if runmode of the test is equal to Y
	/**
	 * @param xls This is the excel sheet which we have to read
	 * @param testCaseName This is to check the test case is runnable or not
	 * @return
	 */
	public static boolean isTestCaseRunnable(XlsReader xls, String testCaseName) {
		boolean isExecutable = false;
		// System.out.println(xls.getRowCount("TestNames"));
		for (int i = 2; i <= xls.getRowCount("TestNames"); i++) {
			// String tcid=xls.getCellData("Test Cases", "TCID", i);
			// String runmode=xls.getCellData("Test Cases", "Runmode", i);
			// System.out.println(tcid +" -- "+ runmode);

			if (xls.getCellData("TestNames", "Test Name", i).equalsIgnoreCase(testCaseName)) {
				if (xls.getCellData("TestNames", "Run Mode", i).equalsIgnoreCase("Y")) {
					isExecutable = true;
				} else {
					isExecutable = false;
				}
			}
		}

		return isExecutable;

	}

	/**
	 * This method for write the report to the excel sheet in test case wise.
	 * @param xls excel sheet name what we are reading
	 * @param testCase we have to pass the excel sheet name to writing the report
	 * @param rowNum This is for update the report in row wise
	 * @param result This is the columns name in this updated the report like pass, fail, skip
	 * @param conf  This is the columns name in this updated the Confirmation number
	 * @param Comments This is the columns name in this updated the comments if the test is failed.
	 * 
	 */
	public static void reportDataSetResult(XlsReader xls, String testCaseName, int rowNum, String result, String conf,
			String Comments) {

		xls.setCellData(testCaseName, "Results", rowNum, result);
		xls.setCellData(testCaseName, "Confirmation Number", rowNum, conf);
		xls.setCellData(testCaseName, "Comments", rowNum, Comments);
	}

	
	/**
	 * This method for get the date from the today date.
	 * @param days we have to pass the number of the days
	 * @return
	 */
	public static String getDatefromToday(int days) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

		// get current date time with Calendar()
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		// System.out.println(dateFormat.format(cal.getTime()));

		return dateFormat.format(cal.getTime());

	}

	// returns true if runmode of the test is equal to Y
	/**
	 * @param xls
	 * @param sheetName
	 * @param id
	 * @return
	 */
	public static int getRowNum(XlsReader xls, String sheetName, String id) {

		for (int i = 2; i <= xls.getRowCount(sheetName); i++) {
			String tcid = xls.getCellData(sheetName, "Test Name", i);
			if (tcid.equals(id)) {
				xls = null;
				return i;
			}
		}
		return -1;
	}

	/**
	 * @return
	 */
	public static String datetime() {
		// Instantiate a Date object
		Date date = new Date();
		// display time and date
		String str = String.format("%tc", date);
		str = str.replace(" ", "");
		return str;
	}

}
