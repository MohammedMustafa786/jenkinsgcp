package main.java.utils.email;

import org.jsoup.Jsoup;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class AHEMMailRestAPI extends TestBase {
	private static String bearerToken = "";
	private static String emailIdentity = "";
	private static String emailname = "";

	/**
	 * This method to get the Bearer Token
	 * 
	 * @throws Exception
	 *
	 */

	public static void getAHEMBearerToken() throws Exception {

		Response response = null;

		try {

			RestAssured.baseURI = "https://hostux.ninja/api/auth/token";

			RequestSpecification httpRequest = RestAssured.given().relaxedHTTPSValidation();

			response = httpRequest.header("Content-Type", "application/json").body("{}").post();

			JsonPath jsonPathEvaluator = response.jsonPath();

			bearerToken = jsonPathEvaluator.getString("token");

		} catch (Exception e) {
			System.err.println(response.body().asPrettyString() + "\n" + response.getStatusCode());
			CommonFunctions.logErrorMessagestopExecution("Error while generating bearer token in ahem's mail");
		}
	}

	/**
	 * This method to get the Email ID
	 * 
	 * @param inboxName
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public static void getEmailId(String inboxName, String subjectName) throws Exception {

		Response response = null;

		boolean flag = false;
		Thread.sleep(10000);
		try {
			RestAssured.baseURI = "https://hostux.ninja/api/mailbox/" + inboxName + "/email";

			RequestSpecification httpRequest = RestAssured.given().relaxedHTTPSValidation();

			response = httpRequest.header("Authorization", "Bearer " + bearerToken).get();

			JsonPath jsonPathEvaluator = response.jsonPath();

			for (int mailSubject = 0; mailSubject < jsonPathEvaluator.getList("emailId").size(); mailSubject++) {

				if (jsonPathEvaluator.getString("subject[" + mailSubject + "]").equalsIgnoreCase(subjectName)) {
					emailIdentity = jsonPathEvaluator.getString("emailId[" + mailSubject + "]");
					flag = true;
					break;
				}
			}

			if (!flag) {
				System.err.println(response.body().asPrettyString() + "\n" + response.getStatusCode());
				CommonFunctions
						.logErrorMessagestopExecution("Mail not received to inbox. Please trouble shoot the error");
			}

		} catch (Exception e) {
			System.err.println(response.body().asPrettyString() + "\n" + response.getStatusCode());
			CommonFunctions.logErrorMessagestopExecution("Error while fetching email Id in ahem's mail");
		}

	}

	/**
	 * This method to fetch the verification code from the response
	 * 
	 * 
	 * @param inboxName
	 * 
	 * @param emailId
	 * 
	 * @throws Exception
	 *
	 */

	public static Response fetchVerificationCode(String inboxName, String emailId) throws Exception {

		Response response = null;

		try {
			RestAssured.baseURI = "https://hostux.ninja/api/mailbox/" + inboxName + "/email/" + emailId + "";

			RequestSpecification httpRequest = RestAssured.given().relaxedHTTPSValidation();

			response = httpRequest.header("Authorization", "Bearer " + bearerToken).get();

			return response;

		} catch (Exception e) {
			System.err.println(response.body().asPrettyString() + "\n" + response.getStatusCode());
			CommonFunctions.logErrorMessagestopExecution("Error while fetching email Id in ahem's mail");
			return null;
		}
	}

	/**
	 * This method to fetch the Navica verification code from the response
	 *
	 *
	 * @param emailLabel
	 *
	 * @throws Exception
	 */

	public static void fetchNavicaCode(String emailLabel, String subjectName) throws Exception {

		getAHEMBearerToken();
		getEmailId(emailLabel, subjectName);
		JsonPath jsonPathEvaluator = fetchVerificationCode(emailLabel, emailIdentity).jsonPath();

		try {
			if (emailname.contains("NAVICA")) {
				navicaVerifyCode = CommonFunctions.regexText("(\\d+)",
						Jsoup.parse(jsonPathEvaluator.getString("textAsHtml")).text());

			} else {
				navicaVerifyCode = CommonFunctions.regexText(">(\\d+){6}<", jsonPathEvaluator.getString("html"))
						.replaceAll(">|<", "");
			}
			CommonFunctions.logMessage("Verification code fetched " + navicaVerifyCode);

		} catch (Exception e) {
			CommonFunctions.logErrorMessagestopExecution("Error while fetching code using ahem mails...");
		}
	}

}
