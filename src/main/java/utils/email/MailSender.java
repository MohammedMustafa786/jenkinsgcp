package main.java.utils.email;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.zeroturnaround.zip.ZipUtil;

import encryptusercredentials.EncryptCredentails;
import main.java.base.TestBase;
import main.java.utils.CommonFunctions;
import main.java.utils.ftp.FTPUploadReport;

/**
 * 
 * @author Mohammed Mustafa
 * 
 * This class sends test execution report via email
 *
 */
public class MailSender extends TestBase {

	/**
	 * This method to send the Report
	 * 
	 * @throws Exception
	 */

	public static void sendReport() throws Exception {

		final String username = new EncryptCredentails()
				.decrypt(CommonFunctions.getPropertyValues().getProperty("username"));
		final String password = new EncryptCredentails()
				.decrypt(CommonFunctions.getPropertyValues().getProperty("password"));
		String fromEmail = new EncryptCredentails()
				.decrypt(CommonFunctions.getPropertyValues().getProperty("username"));
		String toEmail = toReport;

		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");

		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		// Start our mail message
		MimeMessage msg = new MimeMessage(session);
		try {
			msg.setFrom(new InternetAddress(fromEmail));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

			DateFormat dateFormat = new SimpleDateFormat("dd/MMM/YYYY");
			Date date = new Date();
			msg.setSubject(env.toUpperCase() + " BAT " + determineReportStatus() + ": eMed Automation Report "
					+ dateFormat.format(date));

			Multipart emailContent = new MimeMultipart();

			// Text body part
			MimeBodyPart textBodyPart = new MimeBodyPart();

			// Email body data creation
			String htmlBody = emailBodyCreator();

			// Email body message creation
			StringBuilder sb = new StringBuilder();
			sb.append(htmlBody);
			textBodyPart.setContent(sb.toString(), "text/html");

			// Attachment body part.
			MimeBodyPart pdfAttachment = new MimeBodyPart();

			File[] attachFiles = new File(extentReportPath.replace("\\eMed.html", "").replace("/eMed.html", ""))
					.listFiles();

			if (attachFiles.length > 0) {
				// attach file
				attachFile(attachFiles[0], emailContent, pdfAttachment);
				if (attachFiles.length > 1) {
					for (int index = 1; index < attachFiles.length; index++) {
						attachFile(attachFiles[index], emailContent, new MimeBodyPart());
					}
				}
			}

			// Attach body parts
			emailContent.addBodyPart(textBodyPart);
			emailContent.addBodyPart(pdfAttachment);

			// Attach multipart to message
			msg.setContent(emailContent);

			Transport.send(msg);
			System.out.println("Report has mailed successffully. Please check your inbox!!!");
		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method is used to determine the status of report whether passed or
	 * failed
	 * 
	 * @return
	 */
	public static String determineReportStatus() {
		String result = "";

		if (failure == 0) {
			result = "PASS";
		} else {
			result = "FAIL";
		}

		return result;

	}

	/**
	 * Calculate the duration of the total test execution
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String testDuration() {

		String hrs = "00";
		String mins = "00";
		String secs = "00";

		LocalTime end = LocalTime.now();
		String[] timeStamp = Duration.between(startTime, end).toString().replaceAll("P|T", "")
				.replaceAll("\\.(.*)|S", "").replaceAll("M|H", " ").split(" ");

		switch (timeStamp.length) {
		case 1:
			secs = timeStamp[0];
			if (Integer.parseInt(secs) <= 9) {
				secs = "0" + secs;
			}
			break;

		case 2:
			mins = timeStamp[0];
			if (Integer.parseInt(timeStamp[0]) <= 9) {
				mins = "0" + timeStamp[0];
			}

			secs = timeStamp[1];
			if (Integer.parseInt(secs) <= 9) {
				secs = "0" + secs;
			}
			break;

		case 3:
			hrs = timeStamp[0];
			if (Integer.parseInt(timeStamp[0]) <= 9) {
				mins = "0" + timeStamp[0];
			}

			mins = timeStamp[1];
			if (Integer.parseInt(timeStamp[1]) <= 9) {
				mins = "0" + timeStamp[1];
			}

			secs = timeStamp[2];
			if (Integer.parseInt(secs) <= 9) {
				secs = "0" + secs;
			}
			break;

		default:
			break;

		}

		String duration = hrs + "hrs " + mins + "mins " + secs + "secs";
		return duration;
	}

	/**
	 * @author Mohammed Mustafa Method creates the email body structure using html
	 *         tags
	 * @return htmlbody
	 */

	public static String emailBodyCreator() {

		String text = "<h4>Hello Team,</h4>" + "<p>Please find the results for the execution,</p>";

		text = text
				+ "<table style='font-size: 12px; border-collapse: collapse;' width='30%' border='1' bordercolor='black'>"
				+ "<tr bgcolor='gray' align='center' style='color: white;'><td colspan='2'><b>BAT Execution Details</b></td></tr>"
				+ "<tr bgcolor='lightgray'><td><b>Test Name</b></td><td align='center'>BAT</td></tr>"
				+ "<tr><td><b>Triggered by</b></td><td align='center'>" + System.getProperty("user.name") + "</td></tr>"
				+ "<tr bgcolor='lightgray'><td><b>Environment</b></td><td align='center'>" + env.toUpperCase()
				+ "</td></tr>" + "<tr><td><b>Browser</b></td><td align='center'>" + BrowserNeed.toUpperCase()
				+ "</td></tr>" + "<tr bgcolor='lightgray'><td><b>Machine</b></td><td align='center'>"
				+ System.getProperty("os.name") + "</td></tr>";

		text = text
				+ "<tr bgcolor='gray' align='center' style='color: white;'><td colspan='2'><b>Test Case Details</b></td></tr>"
				+ "<tr><td><b>Overall Test Duration</b></td><td align='center'>" + testDuration() + "</td></tr>"
				+ "<tr bgcolor='lightgray'><td><b>Overall Test case Executed</b></td><td align='center'>" + totalTest
				+ "</td></tr>" + "<tr><td><b>Test Passed</b></td><td style='color: green;' align='center'>" + passed
				+ "</td></tr>"
				+ "<tr bgcolor='lightgray'><td><b>Test Failed</b></td><td style='color: red;' align='center'>" + failure
				+ "</td></tr>" + "<tr><td><b>Test Skipped</b></td><td style='color: orange;' align='center'>"
				+ (totalTest - (passed + failure)) + "</td></tr>" + "</table><br>";

		text = text + "<table style='font-size: 12px;' width='100%' border='1' bordercolor='black' align='center'>"
				+ "<tr align='center' bgcolor='lightgray'>" + "<th><b>#</b></th>" + "<th><b>Scenario Name</b></th>"
				+ "<th><b>Scenario Description</b></th>" + "<th><b>Status</b></th>" + "<th><b>Comments</b></th>"
				+ "</tr>";

		for (int emailMsg = 0; emailMsg < scenarioNo.size(); emailMsg++) {

			if (scenarioStatus.get(emailMsg).equalsIgnoreCase("passed")) {
				text = text + "<tr style='color: green; font-size: 12px;'>" + "<td align='center'>"
						+ scenarioNo.get(emailMsg) + "</td>" + "<td>" + scenarioName.get(emailMsg) + "</td>" + "<td>"
						+ scenarioDescription.get(emailMsg) + "</td>" + "<td>" + scenarioStatus.get(emailMsg) + "</td>"
						+ "<td> </td>" + "</tr>";

			} else if (scenarioStatus.get(emailMsg).equalsIgnoreCase("failed")) {
				text = text + "<tr style='color: red; font-size: 12px;'>" + "<td align='center'>"
						+ scenarioNo.get(emailMsg) + "</td>" + "<td>" + scenarioName.get(emailMsg) + "</td>" + "<td>"
						+ scenarioDescription.get(emailMsg) + "</td>" + "<td>" + scenarioStatus.get(emailMsg) + "</td>"
						+ "<td>" + scenarioComments.get(emailMsg) + "</td>" + "</tr>";

			} else if (scenarioStatus.get(emailMsg).equalsIgnoreCase("skipped") && !mailVerbose.equals("2")
					&& !mailVerbose.equals("3")) {
				text = text + "<tr style='color: orange; font-size: 12px;'>" + "<td align='center'>"
						+ scenarioNo.get(emailMsg) + "</td>" + "<td>" + scenarioName.get(emailMsg) + "</td>" + "<td>"
						+ scenarioDescription.get(emailMsg) + "</td>" + "<td>" + scenarioStatus.get(emailMsg) + "</td>"
						+ "<td>" + scenarioComments.get(emailMsg) + "</td>" + "</tr>";
			}

		}

		text = text + "</table>";

		if (mailVerbose.equals("3")) {
			text = text + separateBody();
		}

		text = text + "<p>For more details please look into the extent report attached below.</p>"; 
		//"<br><a href=\""+FTPUploadReport.ftpTransfer()+"\">Click Extent Report</a>";

		return text;
	}

	public static String separateBody() {
		String emailText = "";

		emailText = emailText
				+ "<br><table style='font-size: 12px; border-collapse: collapse;' width='100%' border='1' bordercolor='black'>"
				+ "<tr align='center' bgcolor='orange'>" + "<td colspan='3'><b>Skipped Scenario</b></th>" + "</tr>"
				+ "<tr align='center' bgcolor='lightgray'>" + "<th><b>#</b></th>" + "<th><b>Scenario Name</b></th>"
				+ "<th><b>Scenario Description</b></th>" + "</tr>";

		for (int emailMsg = 0; emailMsg < scenarioNo.size(); emailMsg++) {
			if (scenarioStatus.get(emailMsg).equalsIgnoreCase("skipped")) {
				emailText = emailText + "<tr style='color: black; font-size: 12px;'>" + "<td align='center'>"
						+ scenarioNo.get(emailMsg) + "</td>" + "<td>" + scenarioName.get(emailMsg) + "</td>" + "<td>"
						+ scenarioDescription.get(emailMsg) + "</td>" + "</tr>";
			}
		}
		emailText = emailText + "</table>";
		return emailText;
	}

	/**
	 * This method to attach the file
	 * 
	 * 
	 * @param file
	 * 
	 * @param multipart
	 * 
	 * @param messageBodyPart
	 *
	 * @throws Exception
	 */

	public static void attachFile(File file, Multipart multipart, MimeBodyPart messageBodyPart)
			throws MessagingException {
		DataSource source = new FileDataSource(file);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(file.getName());
		multipart.addBodyPart(messageBodyPart);
	}

	/**
	 * This method for file zip
	 * 
	 * 
	 *
	 * @throws Exception
	 */

	public static void fileZip() throws Exception {

		ZipUtil.pack(new File(extentReportPath.replace("\\eMed.html", "").replace("/eMed.html", "")),
				new File(System.getProperty("user.dir") + "//test-output//eMedReport//eMedEmailReport.zip"));

		Thread.sleep(3000);
		System.out.println("File zipped successfully");
		CommonFunctions.moveFileToSpecificPath(
				System.getProperty("user.dir") + "//test-output//eMedReport//eMedEmailReport.zip",
				extentReportPath.replace("\\eMed.html", "").replace("/eMed.html", "") + "//eMedEmailQAReport.zip");
	}
}
