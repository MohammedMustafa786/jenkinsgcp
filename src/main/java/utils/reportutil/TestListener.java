package main.java.utils.reportutil;

import java.time.LocalTime;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.MediaEntityBuilder;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class TestListener extends TestBase implements ITestListener {

	@Override
	public synchronized void onStart(ITestContext context) {

	}

	@Override
	public synchronized void onFinish(ITestContext context) {

	}

	@Override
	public synchronized void onTestStart(ITestResult result) {

		totalTest++;
		if (totalTest == 1) {
				startTime = LocalTime.now();
		}
		errorLogCount = 0;
	}

	@Override
	public synchronized void onTestSuccess(ITestResult result) {
		if (scenarioStatus.size() < totalTest) {
			try {
				scenarioNo.add(CommonFunctions.getdata("Iteration"));
				scenarioName.add(CommonFunctions.getdata("ScenarioTitle"));
				scenarioDescription.add(CommonFunctions.getdata("Scenario"));
			} catch (Exception e) {
				e.printStackTrace();
			}

			scenarioStatus.add("Passed");
			scenarioComments.add("Pass");
			passed++;
			test.pass("Scenario Passed");

		} else {
			if (!scenarioStatus.get(totalTest - 1).equalsIgnoreCase("failed")) {
				try {
					scenarioNo.add(CommonFunctions.getdata("Iteration"));
					scenarioName.add(CommonFunctions.getdata("ScenarioTitle"));
					scenarioDescription.add(CommonFunctions.getdata("Scenario"));
				} catch (Exception e) {
					e.printStackTrace();
				}

				scenarioStatus.add("Passed");
				scenarioComments.add("Pass");
				passed++;
				test.pass("Scenario Passed");
			}
		}
	}

	@Override
	public synchronized void onTestFailure(ITestResult result) {
		String temp = "";
		try {
			temp = CommonFunctions.getScreenShot();
		} catch (Exception e) {
			e.printStackTrace();
		}
		test.fail("Screen Shot for reference", MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
		test.fail(result.getThrowable());
	}

	@Override
	public synchronized void onTestSkipped(ITestResult result) {

		if (!isExcelSkip) {
			try {

				scenarioNo.add(CommonFunctions.getdata("Iteration"));
				scenarioName.add(CommonFunctions.getdata("ScenarioTitle"));
				scenarioDescription.add(CommonFunctions.getdata("Scenario"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			scenarioStatus.add("Skipped");
			test.skip("This scenario has been skipped by user");
		}
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

}
